resource "hcloud_server" "focalboard" {
  name        = "focalboard"
  image       = "debian-11"
  server_type = "cx11"
  location    = "hel1"
  backups     = "false"
  ssh_keys    = [hcloud_ssh_key.default.id]
  user_data   = data.template_file.user_data.rendered
}

# Definition ssh key from variable
resource "hcloud_ssh_key" "default" {
  name       = "user"
  public_key = var.public_key
}