resource "hcloud_volume" "focalboard_volume" {
  name = "focalboard-data"
  size = 10
  format = "ext4"
  delete_protection = true
  location = "hel1"
}

resource "hcloud_volume_attachment" "focalboard" {
  volume_id = hcloud_volume.focalboard_volume.id
  server_id = hcloud_server.focalboard.id
}
