variable "hcloud_token" {}
variable "hdns_token" {}

variable "public_key" {}

variable "gitlab_remote_state_address" {}
variable "gitlab_username" {}
variable "gitlab_access_token" {}
