resource "hetznerdns_zone" "fablab_rip" {
  name = "fablab.rip"
  ttl  = 86400
}

resource "hetznerdns_record" "focal_fablab_rip" {
  zone_id = hetznerdns_zone.fablab_rip.id
  name    = "focal"
  value   = hcloud_server.focalboard.ipv4_address
  type    = "A"
}

resource "hetznerdns_record" "fablab_rip" {
  zone_id = hetznerdns_zone.fablab_rip.id
  name    = "@"
  value   = "135.181.36.249"
  type    = "A"
}

resource "hetznerdns_record" "lintucam_fablab_rip" {
  zone_id = hetznerdns_zone.fablab_rip.id
  name    = "linutcam"
  value   = "fablab.rip."
  type    = "CNAME"
}

resource "hetznerdns_record" "www_fablab_rip" {
  zone_id = hetznerdns_zone.fablab_rip.id
  name    = "www"
  value   = "fablab.rip."
  type    = "CNAME"
}

resource "hetznerdns_record" "hydrogen_fablab_rip" {
  zone_id = hetznerdns_zone.fablab_rip.id
  name    = "@"
  value   = "hydrogen.ns.hetzner.de."
  type    = "NS"
}

resource "hetznerdns_record" "helium_fablab_rip" {
  zone_id = hetznerdns_zone.fablab_rip.id
  name    = "@"
  value   = "helium.ns.hetzner.de."
  type    = "NS"
}

resource "hetznerdns_record" "oxygen_fablab_rip" {
  zone_id = hetznerdns_zone.fablab_rip.id
  name    = "@"
  value   = "oxygen.ns.hetzner.de."
  type    = "NS"
}
