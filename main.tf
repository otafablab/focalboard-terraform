terraform {
  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "1.32.2"
    }
    hetznerdns = {
      source = "timohirt/hetznerdns"
      version = "1.2.0"
    }
  }
  backend "http" {
    
  }
}

provider "hcloud" {
  token = var.hcloud_token
}

provider "hetznerdns" {
  apitoken = var.hdns_token
}

data "template_file" "user_data" {
  template = templatefile("user-data/cloud-init.ymltpl", {
    public_key = var.public_key
    volume_dev = hcloud_volume.focalboard_volume.linux_device
  })
}

data "terraform_remote_state" "gitlab" {
  backend = "http"

  config = {
    address = var.gitlab_remote_state_address
    username = var.gitlab_username
    password = var.gitlab_access_token
  }
}
